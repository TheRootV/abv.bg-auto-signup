function genRandomNum(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const firstNames = ['Иван', "Петър", "Димитър", "Николай", "Емре", "Христо", "Господин", "Александър", "Борис", "Бойко", "Дарио", "Стефан", "Йордан", "Теодор", "Васил", "Георги", "Гейорги", "Ахмед", "Ахмет", "Мустафа", "Мехмед", "Владислав", "Владимир", "Калоян", "Искрен", "Людмил", "Кристиян", "Ебача на майка ти"]
const lastNames = ["Иванов", "Петров", "Димитров", "Николов", "Христов", "Господинов", "Александров", "Борисов", "Йорданов", "Тодоров", "Василов", "Георгиев", "Гейоргиев", "Ахмедов", "Владимиров", "Владиславов", "Калоянов", "Искренов", "Кристиянов", "Русев", "Неков", "Запотинов"]
// Question and answer max 30 symbols
const question = "I don't care...";
const answer = "F67dMV923PAZ68da1";

function registerAbvUser(username, password)
{
	let body = document.getElementsByTagName('body')[0];
	body.innerHTML = body.innerHTML + "<form style=\"display: none\" data-xml=\"https://passport.abv.bg/app/profiles/validateename\" data-validate=\"true\" class=\"abv-passport abv-clear\" action=\"https://passport.abv.bg/app/profiles/registration\" method=\"post\" autocomplete=\"off\" id=\"constructed\"><input name=\"bday\"><input name=\"bmonth\"><input name=\"byear\"><input name=\"password\"><input name=\"password2\"><input name=\"gender\"><input name=\"username\"><input name=\"fname\"><input name=\"lname\"><input name=\"serviceId\"><input name=\"_phoneRecovery\"><input name=\"mobilePhone\"><input name=\"altemail\"><input name=\"question\"><input name=\"answer\"></form>"
	let form = document.getElementById("constructed");
	form.innerHTML = form.innerHTML + "<textarea id=\"g-recaptcha-response\" name=\"g-recaptcha-response\"></textarea>";
	form.bday.value = genRandomNum(1,28);
	form.bmonth.value = genRandomNum(1,12);
	form.byear.value = genRandomNum(1970,2000);
	form.password.value = password;
	form.password2.value = password;
	form.gender.value = "2";
	form.username.value = username;
	form.fname.value = firstNames[genRandomNum(0, firstNames.length - 1)];
	form.lname.value = lastNames[genRandomNum(0, lastNames.length - 1)];
	form.serviceId.value = "1";
	form._phoneRecovery.value = "on";
	form.mobilePhone.value = "888888888";
	form.altemail.value = username + "@abv.bg";
	form.question.value = question;
	form.answer.value = answer;
	let key = "6LcUPlQUAAAAABs9gkdAwy5iowH5Ywc56251rNE0";
	let url = "https://passport.abv.bg/app/profiles/registration";
	solveRecaptcha(url, key);
	console.log("Please wait. Solving recaptcha...");
}

function callbackFunction(captcha)
{
	let form = document.getElementById("constructed");
	document.getElementById("g-recaptcha-response").value = captcha;
	console.log(captcha);
	form.submit();
}

function solveRecaptcha(url, websitekey) {
    fetch("https://api.anti-captcha.com/createTask", {
        "method": "POST",
        "headers": {
            "content-type": "application/json"
        },
        "body": JSON.stringify({
            "clientKey": "185a342e2a7a7dd588d0da63c7e3552d",
            "task": {
                "type": "NoCaptchaTaskProxyless",
                "websiteURL": url,
                "websiteKey": websitekey,
            }
        })
    }).then(response => {
        return response.json()
    }).then(wtf => {
        setTimeout(() => getAnswer(wtf), 5000)
    });

    function getAnswer(wtf) {
        fetch('https://api.anti-captcha.com/getTaskResult', {
            method: "POST", "headers": {
                "content-type": "application/json"
            },
            "body": JSON.stringify({
                "clientKey": "185a342e2a7a7dd588d0da63c7e3552d",
                "taskId": wtf.taskId
            })
        }).then(res => res.json()).then(j => {
            if (j.status === 'processing') 
			{
				let num = Math.floor(Math.random() * 5);
				switch (num)
				{
					case 0:
						console.log("What are these Pakistanis guys doing?!");
						break;
					case 1:
						console.log("Patience, Luke!");
						break;
					case 2:
						console.log("You wanna some tea?");
						break;
					case 3:
						console.log("Zzzzz...");
						break;
					default:
						console.log("Please wait...");
					
				}
				setTimeout(() => getAnswer(wtf), 3000);
			}
            else {
				console.log(j.status);
				callbackFunction(j.solution.gRecaptchaResponse);
            }
        })
    }
}